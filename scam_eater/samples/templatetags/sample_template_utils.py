from django import template

register = template.Library()


@register.filter
def percentage(value):
    if not isinstance(value, float):
        return "0%"

    return format(value, "%")
