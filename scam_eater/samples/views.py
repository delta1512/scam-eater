import re

from django import forms
from django.core.exceptions import ValidationError
from django.views.generic import DetailView
from django.views.generic.edit import CreateView
from samples.models import Sample


class SampleCollectionForm(forms.ModelForm):
    class Meta:
        model = Sample
        fields = ["raw_sample"]

    raw_sample = forms.CharField(
        required=True,
        strip=True,
        min_length=10,
        max_length=65535,
        label="",
        widget=forms.widgets.Textarea(),
    )

    def clean_raw_sample(self):
        raw_sample = self.cleaned_data["raw_sample"]

        # Check that a website is in the sample
        if not re.findall(Sample.URL_REGEX, raw_sample):
            raise ValidationError(
                "You must provide a scam sample that contains a website"
            )

        return raw_sample


class SampleCollectionFormView(CreateView):
    model = Sample
    form_class = SampleCollectionForm
    template_name = "scam_form.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["initial"] = {"raw_sample": self.request.GET.get("_", "")}
        return kwargs


class SampleDetailView(DetailView):
    model = Sample
    slug_field = "id"
    template_name = "scam_sample.html"
