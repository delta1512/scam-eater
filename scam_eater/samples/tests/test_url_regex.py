from unittest import TestCase

from samples.models import Sample

RGX = Sample.URL_REGEX


class TestURLRegex(TestCase):
    def test_http_website(self):
        self.assertRegex("http://evil.com", RGX)
        self.assertRegex("http://evil.com/", RGX)
        self.assertRegex("http://evil.com/mypath", RGX)
        self.assertRegex("http://evil.com/mypath/", RGX)
        self.assertRegex("http://EVIL.COM/MYPATH/", RGX)

    def test_https_website(self):
        self.assertRegex("https://evil.com", RGX)
        self.assertRegex("https://evil.com/", RGX)
        self.assertRegex("https://evil.com/mypath", RGX)
        self.assertRegex("https://evil.com/mypath/", RGX)
        self.assertRegex("https://EVIL.COM/MYPATH/", RGX)

    def test_ftp_website(self):
        self.assertRegex("ftp://ftp.example.com", RGX)

    def test_www_website(self):
        self.assertRegex("www.duckduckgo.com", RGX)
        self.assertRegex("WWW.DUCKDUCKGO.COM", RGX)

    def test_subdomain_website(self):
        self.assertRegex("https://blog.example.com", RGX)
        self.assertRegex("http://blog.example.com", RGX)
        self.assertRegex("blog.example.com", RGX)
        self.assertRegex("BLOG.EXAMPLE.COM", RGX)
        self.assertRegex("really.long.subdomain.path.example.com", RGX)

    def test_path_with_params(self):
        self.assertRegex("https://example.com/path/to/page?param=1", RGX)
        self.assertRegex(
            "https://www.google.com/imgres?imgurl=https://upload.wikimedia.org/wikipedia/en/thumb/9/9a/Among_Us_cover_art.jpg/220px-Among_Us_cover_art.jpg&tbnid=J8Sv0_997dubsM&vet=1&imgrefurl=https://en.wikipedia.org/wiki/Among_Us&docid=YIfQQwLOJ7CUuM&w=220&h=330&source=sh/x/im/m5/1",
            RGX,
        )
        self.assertRegex(
            "https://www.google.com/search?q=dog&sca_esv=573076625&rlz=1C1CHBF_enCA1044CA1044&tbm=isch&sxsrf=AM9HkKmT5Ku9_Ukzz5q7IQJuHhiM_i-1AA:1697169220809&source=lnms&sa=X&ved=2ahUKEwjhsJ6EkPKBAxVHHzQIHe-cBsoQ_AUoAXoECAIQAw&biw=1280&bih=563&dpr=1.5#imgrc=WnQFluPyt5kKiM",
            RGX,
        )

    def test_ip_address(self):
        self.assertNotRegex("http://192.168.1.1", RGX)
        self.assertNotRegex("192.168.1.1", RGX)
        self.assertNotRegex("https://192.168.1.1", RGX)

    def test_invalid_url(self):
        self.assertNotRegex("invalid-url", RGX)
        self.assertNotRegex("", RGX)
        self.assertNotRegex("Not a URL", RGX)
        self.assertNotRegex(
            "I need some helpwithsomething.please respond ASAP", RGX
        )  # TODO: Fix me
        self.assertNotRegex("This will look like a website.come and show me", RGX)

    def test_special_characters(self):
        self.assertRegex("https://example.com/page%20with%20spaces", RGX)

    def test_long_url(self):
        self.assertRegex("https://" + "a" * 200 + ".com", RGX)
        self.assertRegex("http://" + "a" * 200 + ".com", RGX)
        self.assertRegex("a" * 200 + ".com", RGX)

    def test_domain_with_hyphen(self):
        self.assertRegex("https://sub-domain.example-domain.com", RGX)

    def test_dollar_values(self):
        self.assertNotRegex("Your tax bill is $1500.67. please pay now!!!", RGX)
        self.assertNotRegex("Your tax bill is $1500.67 please pay now!!!", RGX)
        self.assertNotRegex("you have 45.36 in your account please contact now", RGX)
        self.assertNotRegex("bigger value 4500.17", RGX)

    def test_url_in_brackets(self):
        self.assertRegex("(https://example.com/12345)", RGX)
        self.assertRegex("pease visit the site (my.site.com)", RGX)

    def test_real_positives(self):
        # test with some real data
        # DO NOT GO TO THESE WEBSITES, these are likely scams
        self.assertRegex("https://ai.linkt-gov.club", RGX)
        self.assertRegex("https://lihi1.com/wgQx7", RGX)
        self.assertRegex("https://h0y.life/0ykjZ", RGX)
        self.assertRegex("https://bit.ly/paypa1s", RGX)
        self.assertRegex("http://aun.buzz/YDNP", RGX)
        self.assertRegex("http://BGTt74.org", RGX)
        self.assertRegex("unitcan-001-site1.ftempurl.com/USAU/", RGX)
        self.assertRegex("http://tinyurl.com/Bautlk", RGX)
        self.assertRegex("www.auspost-redirection.digital", RGX)
        self.assertRegex(
            "https://drive.google.com/file/d/1HG6Oy6pXtHZJK7QwUCFhi8MjGGw-1AiU/view?pli=1",
            RGX,
        )

    def test_real_negatives(self):
        # test with some real data
        self.assertNotRegex("http://1500.00", RGX)
        self.assertNotRegex("325.02", RGX)
        self.assertNotRegex("asP.s.:", RGX)
        self.assertNotRegex("P.S.", RGX)
        self.assertNotRegex("t.Jason", RGX)  # TODO: Fix me
        self.assertNotRegex("http://t.Jason", RGX)
        self.assertNotRegex("delays.Please", RGX)
