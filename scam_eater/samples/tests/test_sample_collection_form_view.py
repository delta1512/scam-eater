from django.test import TestCase
from django.urls import reverse
from samples.models import Sample


class SampleCollectionFormViewTests(TestCase):
    def setUp(self):
        self.url = reverse("sample-collection-form")
        self.raw_sample_data = "Your raw sample data goes here. https://example.com"

    def test_get_webpage(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_get_with_raw_sample_in_url(self):
        response = self.client.get(f"{self.url}?_={self.raw_sample_data}")

        form = response.context["form"]

        self.assertEqual(response.status_code, 200)
        self.assertIn("raw_sample", form.initial)
        self.assertEqual(self.raw_sample_data, form.initial["raw_sample"])

    def test_form_submission(self):
        response = self.client.post(
            self.url,
            {
                "raw_sample": self.raw_sample_data,
            },
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(Sample.objects.count(), 1)

    def test_invalid_submission_no_website(self):
        response = self.client.post(
            self.url,
            {
                "raw_sample": "This is an invalid submission as it has no website",
            },
        )

        form = response.context["form"]

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Sample.objects.count(), 0)
        self.assertIn("raw_sample", form.errors)

    def test_invalid_submission_not_long_enough(self):
        response = self.client.post(
            self.url,
            {
                "raw_sample": "hello",
            },
        )

        form = response.context["form"]

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Sample.objects.count(), 0)
        self.assertIn("raw_sample", form.errors)
