from django.contrib import admin
from django.forms import ModelForm, ValidationError

from samples.models import Sample, Website, DomainName, ReportingActivity


class WebsiteInline(admin.TabularInline):
    model = Website

    fields = ["id", "domain_name", "url"]
    readonly_fields = ["id", "domain_name", "url"]

    def has_add_permission(self, *args, **kwargs):
        return False
    
    def has_delete_permission(self, *args, **kwargs):
        return False


class ReportingActivityInline(admin.TabularInline):
    model = ReportingActivity

    fields = ["id", "title", "description", "activity_date"]


class SampleAdminForm(ModelForm):
    class Meta:
        model = Sample
        fields = "__all__"
    
    def clean_scam_score(self):
        scam_score = self.cleaned_data["scam_score"]

        if scam_score is None:
            return scam_score
        
        if not isinstance(scam_score, float):
            raise ValidationError("Invalid scam score type provided")
        
        if not (0 <= scam_score <= 1):
            raise ValidationError("Invalid scam score provided")
        
        return scam_score


@admin.register(Sample)
class SampleAdmin(admin.ModelAdmin):
    form = SampleAdminForm

    list_display = ["id", "sample_hash", "status", "date_created"]

    inlines = [WebsiteInline, ReportingActivityInline]


@admin.register(Website)
class WebsiteAdmin(admin.ModelAdmin):
    list_display = ["id", "url", "date_created"]


@admin.register(DomainName)
class DomainNameAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "date_created"]


@admin.register(ReportingActivity)
class ReportingActivityAdmin(admin.ModelAdmin):
    list_display = ["id", "sample", "title", "date_created"]