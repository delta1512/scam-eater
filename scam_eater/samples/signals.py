import re
from urllib.parse import urlparse

from django.db.models.signals import post_save
from django.dispatch import receiver

from samples.models import Sample, Website, DomainName


@receiver(post_save, sender=Sample)
def process_urls(sender, instance: Sample, created: bool, **kwargs):
    if not created:
        return
    
    matches = re.findall(Sample.URL_REGEX, str(instance.raw_sample))
    websites = []

    for match in matches:
        # For urllib to parse a hostname, we need the scheme
        if not "://" in match:
            match = "http://" + match
        
        url = urlparse(match)
        domain, _ = DomainName.objects.get_or_create(name=url.hostname)
        websites.append(Website(sample=instance, url=match, domain_name=domain))
    
    Website.objects.bulk_create(websites, batch_size=500)
    
    
