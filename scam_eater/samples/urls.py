from django.urls import path

from . import views

urlpatterns = [
    path("", views.SampleCollectionFormView.as_view(), name="sample-collection-form"),
    path("sample/<slug:slug>/", views.SampleDetailView.as_view(), name="sample-detail"),
]
