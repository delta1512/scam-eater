import hashlib

from django.db import models
from django.urls import reverse


class Sample(models.Model):
    class Meta:
        indexes = [
            models.Index(fields=("sample_hash",)),
            models.Index(fields=("date_created",)),
            models.Index(fields=("date_modified",)),
        ]

    URL_REGEX = r"\b(?:https?:\/\/)?[-a-zA-Z0-9_\.]{1,256}\.[a-zA-Z]{2,7}\b[-a-zA-Z0-9()!@:%_\+.~#?&\/=]*\b"

    PLATFORM_EMAIL = "email"
    PLATFORM_PHONE = "phone"
    PLATFORM_TELEGRAM = "telegram"
    PLATFORM_DISCORD = "discord"
    PLATFORM_FACEBOOK = "facebook"
    PLATFORM_MESSENGER = "messenger"
    PLATFORM_WHATSAPP = "whatsapp"
    PLATFORM_TWITTER = "twitter"
    PLATFORM_REDDIT = "reddit"
    PLATFORMS = (
        (PLATFORM_EMAIL, "Email"),
        (PLATFORM_PHONE, "Phone/Mobile"),
        (PLATFORM_TELEGRAM, "Telegram"),
        (PLATFORM_DISCORD, "Discord"),
        (PLATFORM_FACEBOOK, "Facebook/Meta"),
        (PLATFORM_MESSENGER, "Messenger"),
        (PLATFORM_WHATSAPP, "Whatsapp"),
        (PLATFORM_TWITTER, "Twitter/X"),
        (PLATFORM_REDDIT, "Reddit"),
    )

    STATUS_NEW = "new"
    STATUS_REPORTED = "reported"
    STATUS_INVALID = "invalid"
    STATUS_SUCCESS = "success"
    STATUS_CANNOT_PROCEED = "no_activity_success"
    STATUS_DUPLICATED = "duplicate"
    STATUS_NOT_ENOUGH_INFO = "not_enough_info"
    STATUS_STATES = (
        (STATUS_NEW, "New"),
        (STATUS_INVALID, "Invalid"),
        (STATUS_REPORTED, "Report(s) made"),
        (STATUS_SUCCESS, "Successfully taken-down"),
        (STATUS_CANNOT_PROCEED, "Can't take action"),
        (STATUS_DUPLICATED, "Duplicate"),
        (STATUS_NOT_ENOUGH_INFO, "Not enough information"),
    )
    STATUS_EXPLANATIONS = {
        STATUS_NEW: "We've received your scam sample and are yet to start processing it. Come back later to see the results ⌚",
        STATUS_REPORTED: "We've started reporting your scam to the relevant places. Hopefully it will be taken down soon! 🧑‍💻",
        STATUS_INVALID: "Sorry, but it looks like the data you gave us was invalid. We can't work with this 😕",
        STATUS_SUCCESS: "Hooray! 🎉 The scam has been taken down, thanks for reporting it to us! 😁",
        STATUS_CANNOT_PROCEED: "We tried, but it looks like we can't do much here. 😕",
        STATUS_DUPLICATED: "Looks like this sample is a duplicate of another one we found. But don't worry, your submission will be used to count how widespread this scam is! 📊",
        STATUS_NOT_ENOUGH_INFO: "We are not sure if this is a scam. There isn't enough information to take any action on this one, sorry 🙁",
    }

    STATUS_REASON_CONN_FAILED = "connection_failed"
    STATUS_REASON_NO_LINKS = "no_links_provided"
    STATUS_REASON_NO_REPORTING = "no_reporting_mechanism"
    STATUS_REASON_DORMANT = "alive_no_longer_active"
    STATUS_REASONS = (
        (STATUS_REASON_CONN_FAILED, "Cannot connect to website"),
        (STATUS_REASON_NO_LINKS, "No websites provided in sample"),
        (STATUS_REASON_NO_REPORTING, "No way to report abuse"),
        (STATUS_REASON_DORMANT, "Able to connect but scam is no longer there"),
    )

    raw_sample = models.TextField(blank=False, null=False)
    sample_hash = models.CharField(max_length=256, blank=False, null=False)
    sample_scammer_origin = models.CharField(
        max_length=512,
        blank=True,
        null=True,
        default=None,
        help_text="The email, phone or other ID of the scammer",
    )
    sample_platform_origin = models.CharField(
        max_length=64,
        blank=True,
        null=True,
        choices=PLATFORMS,
        help_text="The communication medium where this scam was found",
    )
    status = models.CharField(
        max_length=64,
        blank=False,
        null=False,
        choices=STATUS_STATES,
        default=STATUS_NEW,
    )
    status_reason = models.CharField(
        max_length=64, blank=True, null=True, choices=STATUS_REASONS, default=None
    )
    scam_score = models.FloatField(
        blank=False,
        null=True,
        default=None,
        help_text="How likely this is to be a scam. 0=not a scam, 1=scam.",
    )

    reporter_ip = models.CharField(
        max_length=46,
        blank=True,
        null=True,
        help_text="The IP address of the user who reported this",
    )
    reporter_country_code = models.CharField(max_length=2, blank=True, null=True)

    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.sample_hash = hashlib.sha1(self.raw_sample.encode()).hexdigest()
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("sample-detail", kwargs={"slug": self.pk})

    @property
    def summary_text(self) -> str:
        return self.STATUS_EXPLANATIONS.get(
            self.status,
            "There's supposed to be a description here but it looks like we don't have one. Oops! 😅",
        )

    @property
    def status_reason_text(self) -> str:
        for status_reason_entry in self.STATUS_REASONS:
            if status_reason_entry[0] == self.status_reason:
                return status_reason_entry[1]
        return ""

    @property
    def status_text(self) -> str:
        for status_entry in self.STATUS_STATES:
            if status_entry[0] == self.status:
                if self.status_reason:
                    return f"{status_entry[1]} - {self.status_reason_text}"
                else:
                    return status_entry[1]
        return "N/A"

    @property
    def sample_emoji(self) -> str:
        if self.scam_score is None:
            return "❓"

        if 0 <= self.scam_score <= 0.25:
            return "🤔"
        elif 0.25 < self.scam_score <= 0.5:
            return "🤨"
        elif 0.5 < self.scam_score <= 0.75:
            return "⚠"
        elif 0.75 < self.scam_score <= 1:
            return "⛔"
        else:
            return "😶‍🌫️"

    @property
    def reporting_activities(self):
        return ReportingActivity.objects.filter(sample=self)


class DomainName(models.Model):
    class Meta:
        indexes = [models.Index(fields=("name",))]

    name = models.CharField(max_length=256, blank=False, null=False)
    reported_for_abuse = models.BooleanField(blank=False, null=False, default=False)

    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


class Website(models.Model):
    class Meta:
        indexes = [models.Index(fields=("date_created",))]

    sample = models.ForeignKey(to=Sample, on_delete=models.CASCADE)
    domain_name = models.ForeignKey(
        to=DomainName, null=True, on_delete=models.SET_NULL, default=None
    )

    url = models.CharField(max_length=512, blank=False, null=False)

    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


class ReportingActivity(models.Model):
    class Meta:
        indexes = [models.Index(fields=("date_created",))]

    sample = models.ForeignKey(to=Sample, on_delete=models.CASCADE)

    title = models.CharField(max_length=128, blank=False, null=False)
    description = models.CharField(max_length=128, blank=True, null=True)
    activity_date = models.DateTimeField(
        blank=True,
        null=True,
        default=None,
        help_text="The date in which the activity actually happened",
    )

    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    @property
    def time_occurred(self):
        if self.activity_date:
            return self.activity_date
        return self.date_created
