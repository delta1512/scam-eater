# Generated by Django 4.2.5 on 2023-10-11 22:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('samples', '0007_alter_sample_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='reportingactivity',
            name='activity_date',
            field=models.DateTimeField(blank=True, default=None, help_text='The date in which the activity actually happened', null=True),
        ),
    ]
