# Generated by Django 4.2.5 on 2023-10-06 23:44

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("samples", "0005_sample_scam_score_alter_sample_reporter_ip_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="sample",
            name="scam_score",
            field=models.FloatField(
                default=None,
                help_text="How likely this is to be a scam. 0=not a scam, 1=scam.",
                null=True,
            ),
        ),
        migrations.AlterField(
            model_name="sample",
            name="status_reason",
            field=models.CharField(
                blank=True,
                choices=[
                    ("connection_failed", "Cannot connect to website"),
                    ("no_links_provided", "No websites provided in sample"),
                    ("no_reporting_mechanism", "No way to report abuse"),
                    (
                        "alive_no_longer_active",
                        "Able to connect but scam is no longer there",
                    ),
                ],
                default=None,
                max_length=64,
                null=True,
            ),
        ),
    ]
